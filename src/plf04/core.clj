(ns plf04.core)
;string-E
;lineal
(defn string-E-1
  [s]
  (letfn [(g [c m]
            (cond
              (and (= c \e) (>= (m :c) 3)) {:r false :c (inc (m :c))}
              (and (= c \e) (>= (m :c) 0)) {:r true :c (inc (m :c))}
              :else m))
          (f [xs]
            (if (empty? xs)
              {:r false :c 0}
              (g (first xs) (f (rest xs)))))]
    ((f s) :r)))


(string-E-1 "Hello")
(string-E-1 "Heelle")
(string-E-1 "Heelele")
(string-E-1 "Hll")
(string-E-1 "e")
(string-E-1 "")

;cola
(defn string-E-2
  [s]
  (letfn [(g [x]
            (and (>= x 1) (<= x 3)))
          (h [x y]
            (if (= x \e) (inc y) y))
          (f [xs acc]
            (if (empty? xs)
              (g acc)
              (f (rest xs)
                 (h (first xs) acc))))]
    (f s 0)))

(string-E-2 "Hello")
(string-E-2 "Heelle")
(string-E-2 "Heelele")
(string-E-2 "Hll")
(string-E-2 "e")
(string-E-2 "")

;string-times
;lineal
(defn string-times-1
  [x y]
  (letfn [(f [xs ys]
            (if (zero? ys)
              ""
              (str xs (f xs (dec ys)))))]
    (f x y)))

(string-times-1 "Hi", 2)
(string-times-1 "Hi", 3)
(string-times-1 "Hi", 1)
(string-times-1 "Hi", 0)
(string-times-1 "Hi", 5)
(string-times-1 "Oh Boy!", 2)
(string-times-1 "x", 4)
(string-times-1 "", 4)
(string-times-1 "code", 2)
(string-times-1 "code", 3)

;Cola
(defn string-times-2
  [x y z]
  (letfn [(f [xs ys acc]
            (if (zero? ys)
              acc
              (str xs (f xs (dec ys) acc))))]
    (f x y z)))

(string-times-2 "Hi" 2 "")
(string-times-2 "Hi" 3 "")
(string-times-2 "Hi" 1 "")
(string-times-2 "Hi" 0 "")
(string-times-2 "Hi" 5 "")
(string-times-2 "Oh Boy!" 2 "")
(string-times-2 "x" 4 "")
(string-times-2 "" 4 "")
(string-times-2 "code" 2 "")
(string-times-2 "code" 3 "")

;front-Times
;lineal
(defn front-times-1
  [x y]
  (letfn [(f [xs ys]
            (if (or (zero? ys) (empty? xs))
              ""
              (if (>= (count xs) 3)
                (str (subs xs 0 3) (f xs (dec ys)))
                (str xs (f xs (dec ys)))
              ;((str (first xs)) (f (rest xs) (dec ys)))
                )))]
    (f x y)))

(front-times-1 "Chocolate" 2)
(front-times-1 "Chocolate" 3)
(front-times-1 "Abc" 3)
(front-times-1 "Ab" 4)
(front-times-1 "A" 4)
(front-times-1 "" 4)
(front-times-1 "Abc" 0)

;Cola
(defn front-times-2
  [x y z]
  (letfn [(f [xs ys acc]
            (if (or (zero? ys) (empty? xs))
              acc
              (if (>= (count xs) 3)
                (str (subs xs 0 3) (f xs (dec ys) acc))
                (str xs (f xs (dec ys) acc))
              ;((str (first xs)) (f (rest xs) (dec ys)))
                )))]
    (f x y z)))

(front-times-2 "Chocolate" 2 "")
(front-times-2 "Chocolate" 3 "")
(front-times-2 "Abc" 3 "")
(front-times-2 "Ab" 4 "")
(front-times-2 "A" 4 "")
(front-times-2 "" 4 "")
(front-times-2 "Abc" 0 "")

;count-XX
;lineal
(defn count-XX-1
  [x]
  (letfn [(f [xs]
            (if (empty? xs)
              0
              (if (and (= \x (first xs)) (= \x (first (rest xs))))
                (inc (f (rest xs)))
                (f (rest xs)))))]
    (f x)))

(count-XX-1 "abcxx")
(count-XX-1 "xxx")
(count-XX-1 "xxxx")
(count-XX-1 "abc")
(count-XX-1 "Hello there")
(count-XX-1 "Hexxo thxxe")
(count-XX-1 "")
(count-XX-1 "Kittens")
(count-XX-1 "Kittensxxx")

;cola
(defn count-XX-2
  [x y]
  (letfn [(f [a acc]
            (if (empty? a)
              acc
              (if (and (= \x (first a)) (= \x (first (rest a))))
                (f (rest a) (+ acc 1))
                (f (rest a) acc))))]
    (f x y)))

(count-XX-2 "abcxx" 0)
(count-XX-2 "xxx" 0)
(count-XX-2 "xxxx" 0)
(count-XX-2 "abc" 0)
(count-XX-2 "Hello there" 0)
(count-XX-2 "Hexxo thxxe" 0)
(count-XX-2 "" 0)
(count-XX-2 "Kittens" 0)
(count-XX-2 "Kittensxxx" 0)

;string-splosion
;lineal
(defn string-splosion-1
  [x]
  (letfn [(f [xs ys]
            (if (or (>= ys (count xs)) (empty? xs))
              ""
              (str (subs xs 0 (inc ys)) (f xs (inc ys)))))]
    (f x 0)))

(string-splosion-1 "Code")
(string-splosion-1 "abc")
(string-splosion-1 "ab")
(string-splosion-1 "x")
(string-splosion-1 "fade")
(string-splosion-1 "There")
(string-splosion-1 "Kitten")
(string-splosion-1 "Bye")
(string-splosion-1 "Good")
(string-splosion-1 "Bad")
;cola
(defn string-splosion-2
  [x y]
  (letfn [(f [xs acc]
            (if (or (>= acc (count xs)) (empty? xs))
              ""
              (str (subs xs 0 (inc acc)) (f xs (inc acc)))))]
    (f x y)))

(string-splosion-2 "Code" 0)
(string-splosion-2 "abc" 0)
(string-splosion-2 "ab" 0)
(string-splosion-2 "x" 0)
(string-splosion-2 "fade" 0)
(string-splosion-2 "There" 0)
(string-splosion-2 "Kitten" 0)
(string-splosion-2 "Bye" 0)
(string-splosion-2 "Good" 0)
(string-splosion-2 "Bad" 0)

;array-123
;lineal
(defn array-123-1
  [x]
  (letfn [(f [xs]
            (if (empty? xs)
              false
              (if
               (and
                (= 1 (first xs))
                (= 2 (first (rest xs)))
                (= 3 (first (rest (rest xs)))))
                true
                (f (rest xs)))))]
    (f x)))

(array-123-1 [1, 1, 2, 3, 1])
(array-123-1 [1, 1, 2, 4, 1])
(array-123-1 [1, 1, 2, 1, 2, 3])
(array-123-1 [1, 1, 2, 1, 2, 1])
(array-123-1 [1, 2, 3, 1, 2, 3])
(array-123-1 [1, 2, 3])
(array-123-1 [1, 1, 1])
(array-123-1 [1, 2])
(array-123-1 [1])
(array-123-1 [])
(array-123-1 [1 2 3 4 5 6])

;Cola
(defn array-123-2
  [x y]
  (letfn [(f [xs acc]
            (if (empty? xs)
              (if (>= acc 1)
                true
                false)
              (if
               (and
                (= 1 (first xs))
                (= 2 (first (rest xs)))
                (= 3 (first (rest (rest xs)))))
                (f (rest xs) (+ acc 1))
                (f (rest xs) acc))))]
    (f x y)))

(array-123-2 [1, 1, 2, 3, 1] 0)
(array-123-2 [1, 1, 2, 4, 1] 0)
(array-123-2 [1, 1, 2, 1, 2, 3] 0)
(array-123-2 [1, 1, 2, 1, 2, 1] 0)
(array-123-2 [1, 2, 3, 1, 2, 3] 0)
(array-123-2 [1, 2, 3] 0)
(array-123-2 [1, 1, 1] 0)
(array-123-2 [1, 2] 0)
(array-123-2 [1] 0)
(array-123-2 [] 0)

;astringX
;lineal
(defn string-X-1
  [x]
  (letfn [(f [xs ys]
            (if (empty? xs)
              ""
              (if
               (zero? ys)
                (str (first xs) (f (rest xs) (inc ys)))
                (if
                 (and (= (first xs) \x) (> (count xs) 1))
                  (f (rest xs) (inc ys))
                  (str (first xs) (f (rest xs) (inc ys)))))))]
    (f x 0)))

(string-X-1 "xxHxix")
(string-X-1 "abxxxcd")
(string-X-1 "xabxxxcdx")
(string-X-1 "xKittenx")
(string-X-1 "Hello")
(string-X-1 "xx")
(string-X-1 "x")
(string-X-1 "")
;cola
(defn string-X-2
  [x]
  (letfn [(f [xs ys acc]
            (if (empty? xs)
              acc
              (if (zero? ys)
                (str (first xs)
                     (f (rest xs) (inc ys) (str acc)))
                (if (and
                     (= \x (first xs))
                     (> (count xs) 1))
                  (f (rest xs) (inc ys) (str acc))
                  (str (first xs)
                       (f (rest xs) (inc ys) acc))))))]
    (f x 0 "")))

(string-X-2 "xxHxix")
(string-X-2 "abxxxcd")
(string-X-2 "xabxxxcdx")
(string-X-2 "xKittenx")
(string-X-2 "Hello")
(string-X-2 "xx")
(string-X-2 "x")
(string-X-2 "")
;altPairs
;lineal
(defn altPairs-1
  [x]
  (letfn [(f [xs ys]
            (if (== (count xs) ys)
              ""
              (if (or (zero?  ys) (== ys 1) (== ys 4) (== ys 5) (== ys 8) (== ys 9))
                (str
                 (subs xs ys (inc ys))
                 (f x (inc ys)))
                (f x (inc ys)))))]
    (f x 0)))

(altPairs-1 "kitten")
(altPairs-1 "Chocolate")
(altPairs-1 "CodingHorror")
(altPairs-1 "yak")
(altPairs-1 "ya")
(altPairs-1 "y")
(altPairs-1 "")
(altPairs-1 "ThisThatTheOther")
;cola
(defn altPairs-2
  [x y]
  (letfn [(f [xs ys acc]
            (if (== (count xs) ys)
              acc
              (if (or (zero? ys) (== ys 1) (== ys 4) (== ys 5) (== ys 8) (== ys 9))
                (f xs (inc ys) (str acc (subs xs ys (inc ys))))
                (f xs (inc ys) acc))))]
    (f x 0 y)))

(altPairs-2 "kitten" "")
(altPairs-2 "Chocolate" "")
(altPairs-2 "CodingHorror" "")
(altPairs-2 "yak" "")
(altPairs-2 "ya" "")
(altPairs-2 "y" "")
(altPairs-2 "" "")
(altPairs-2 "ThisThatTheOther" "")
;string-Yak
;lienal
(defn string-Yak-1
  [x]
  (letfn [(f [xs]
            (if (empty? xs)
              ""
              (if
               (and (= (first xs) \y)
                    (= (first (rest (rest xs))) \k)
                    (> (count xs) 1))
                (f (rest (rest (rest  xs))))
                (str (first xs) (f (rest xs))))))]
    (f x)))

(string-Yak-1 "yakpak")
(string-Yak-1 "pakyak")
(string-Yak-1 "yak123ya")
(string-Yak-1 "yak")
(string-Yak-1 "yakxxxyak")
(string-Yak-1 "HiyakHi")
(string-Yak-1 "xxxyakyyyakzzz")
;cola
(defn string-Yak-2
  [x y]
  (letfn [(f [xs acc]
            (if (empty? xs)
              ""
              (if
               (and (= (first xs) \y)
                    (= (first (rest (rest xs))) \k)
                    (> (count xs) 1))
                (f (rest (rest (rest xs))) (inc acc))
                (str (first xs) (f (rest xs) (inc acc))))))]
    (f x y)))

(string-Yak-2 "yakpak" 0)
(string-Yak-2 "pakyak" 0)
(string-Yak-2 "yak123ya" 0)
(string-Yak-2 "yak" 0)
(string-Yak-2 "yakxxxyak" 0)
(string-Yak-2 "HiyakHi" 0)
(string-Yak-2 "xxxyakyyyakzzz" 0)
;has271